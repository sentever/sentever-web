#!/bin/bash
#This script launches a docker container with the application, and runs the ng test command

docker run -ti -p 4201:4201 -v /home/martinomburajr/dev/sentever/:/home/ angular:1.0 /bin/ash -c "cd home/sentever-web; ng test"
