// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDRZw0ji29JxNVDRFsJywNFo2Q8KTLfFts",
    authDomain: "sentever-dev.firebaseapp.com",
    databaseURL: "https://sentever-dev.firebaseio.com",
    projectId: "sentever-dev",
    storageBucket: "sentever-dev.appspot.com",
    messagingSenderId: "523417273703"
  }
};
