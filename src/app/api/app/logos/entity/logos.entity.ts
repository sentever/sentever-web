export class LogosEntity {
        static  COLLECTION: string = "logos";
        key: string;
        pos: string;
        id:string;
        isArchived: boolean = false;
        ready:boolean;
        uploadDate: Date;
        isInitializable: boolean;
        availableDate:Date;
        favorites: number;

        //Interface types
        word: IWord;
        wordVariants: Array<IWordVariant>
        synonyms: Array<ISynonym>
        etymologys: Array<IEtymology>;
        definitions: Array<IDefinition>;
        examples: Array<IExample>;
        anecdotes: Array<IAnecdote>
        pronunciations: Array<IPronunciation>;
        frequency: IFrequency;
        languages: Array<ILanguage>;
        quotes: Array<IQuote>;
        origins: Array<IOrigin>;
        bannerURL: IImage
        imageURLs: Array<IImage>;

        public constructor() {
            this.word = {word: "", source: ""};
            this.pos = "";
            this.key = "";
            this.availableDate = new Date();
            this.bannerURL = {image: "", author: "", authorLink: "", source: ""}
            this.imageURLs = new Array<IImage>();
            this.imageURLs.push({image: "", author: "", authorLink: "", source: ""})

            this.frequency = {frequency: 0, source: ""};
            this.wordVariants = new Array<IWordVariant>();
            this.wordVariants.push({wordVariant: "", wordVariantInfo: "", source: ""});

            this.etymologys = new Array<IEtymology>();
            this.etymologys.push({etymology: "", source: ""});

            this.synonyms = new Array<ISynonym>();
            this.synonyms.push({synonym: "", source: ""});

            this.definitions = new Array<IDefinition>();
            this.definitions.push({definition: "", source: ""});

            this.examples = new Array<IExample>();
            this.examples.push({example: "", source: ""});

            this.anecdotes = new Array<IAnecdote>();
            this.anecdotes.push({anecdote: "", source: ""});

            this.quotes = new Array<IQuote>();
            this.quotes.push({quote: "", source: ""});

            this.origins = new Array<IOrigin>();
            this.origins.push({origin: "", source: ""});

            this.pronunciations = new Array<IPronunciation>();
            this.pronunciations.push({pronunciation: "", source: ""});

            this.languages = new Array<ILanguage>();
            this.languages.push({language: "", source: ""});

            this.uploadDate = new Date();
            this.isInitializable = false;

            this.favorites = 0;
        }

        public toObject(): {} {
                let obj = {};
                obj['word'] = this.word;
                obj['synonyms'] = this.synonyms;
                obj['wordVariants'] = this.wordVariants;
                obj['definitions'] = this.definitions;
                obj['examples'] = this.examples;
                obj['etymology'] = this.etymologys;
                obj['quotes'] = this.quotes;
                obj['frequency'] = this.frequency;
                obj['wordVariants'] = this.wordVariants;
                obj['favorites'] = this.favorites;
                obj['pos'] = this.pos;
                obj['anecdotes'] = this.anecdotes;
                obj['bannerURL'] = this.bannerURL;
                obj['imageURLs'] = this.imageURLs;
                obj['isInitializable'] = this.isInitializable;
                obj['pronunciations'] = this.pronunciations;
                obj['origins'] = this.origins;
                obj['languages'] = this.languages;
                obj['uploadDate'] = Math.ceil((Date.now()/1000));
                return obj;
        }

        public fromJSON(key: string,obj: {}) {
          if(key) {this.key = key}
           if(obj['word']) { this.word = obj['word']} ;
           if(obj['definitions'] ){ this.definitions = obj['definitions']}
           if(obj['examples'] ){ this.examples =  obj['examples']}
           if(obj['etymology'] ){ this.etymologys =  obj['etymology']}
           if(obj['bannerURL']){this.bannerURL = obj['bannerURL']}
           if(obj['imageURLs']){this.imageURLs = obj['imageURLs']}
           if(obj['isInitializable']){this.isInitializable = obj['isInitializable']}
           if(obj['pronunciations']){ this.pronunciations = obj['pronunciations']};
           if(obj['origins']){this.origins = obj['origins']};
           if(obj['languages']){this.languages = obj['languages'] };
           if(obj['pos'] ){ this.pos = obj['pos']}
           if(obj['anecdotes'] ){ this.anecdotes = obj['anecdotes']};
        }

        public static POS: Array<string> = new Array<string>('noun','adjective','verb','adverb','pronoun','preposition','interjection', 'conjunction')
}

export interface IWordVariant extends ISourceable{
    wordVariant: string;
    wordVariantInfo: string;
}

export interface ISynonym extends ISourceable {
    synonym: string;
}

export interface IDefinition extends ISourceable{
    definition: string;
}

export interface IQuote extends ISourceable {
    quote: string;
}

export interface IExample extends ISourceable {
    example: string;
}

export interface IAnecdote extends ISourceable {
    anecdote: string;
}

export interface IEtymology extends ISourceable {
    etymology: string;
}

export interface IFrequency extends ISourceable {
    frequency: number;
}

export interface IOrigin extends ISourceable {
    origin: string;
}

export interface IPronunciation extends ISourceable {
    pronunciation: string;
}

export interface ILanguage extends ISourceable {
    language: string;
}

export interface IWord extends ISourceable {
    word: string;
}

export interface ISourceable {
    source: string;
}

export interface IImage {
    image: string;
    author:string;
    authorLink:string;
    source: string;
}

