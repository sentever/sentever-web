import {IEtymology, IWord, LogosEntity} from "./logos.entity";

export class LogosValidation {
    private mLogos: LogosEntity;
    public constructor(logos: LogosEntity) {
        this.mLogos = logos;
    }

    public validateAll(): Array<string> {
        let arr = new Array<string>();
        const word = this.validateWord();
        const definition = this.validateDefinition();
        const pos = this.validatePOS();
        const anecdote = this.validateAnecdote();
        const banner = this.validateBannerURL();
        const etymology = this.validateEtymology();
        const example = this.validateExample();
        const imageURLs = this.validateImageURLs();
        const language = this.validateLanguages();
        const origin = this.validateOrigins();
        const pronuncia = this.validatePronunciations();
        const quotes = this.validateQuotes();
        const synonyms = this.validateSynonyms();
        const wordVari = this.validateWordVariants();

        if(word) {arr.push(word)}
        if(definition) {arr.push(definition)}
        if(pos) {arr.push(pos)}
        if(anecdote) {arr.push(anecdote)}
        if(banner) {arr.push(banner)}
        if(etymology) {arr.push(etymology)}
        if(example) {arr.push(example)}
        if(imageURLs) {arr.push(imageURLs)}
        if(language) {arr.push(language)}
        if(origin) {arr.push(origin)}
        if(pronuncia) {arr.push(pronuncia)}
        if(quotes) {arr.push(quotes)}
        if(synonyms) {arr.push(synonyms)}
        if(wordVari) {arr.push(wordVari)}
        return arr;
    }

    public cleanAll(): LogosEntity {
        this.cleanAnecdotes()
        this.cleanBannerURL()
        this.cleanDefinition()
        this.cleanExample()
        this.cleanImageURLs()
        this.cleanLanguages()
        this.cleanOrigins()
        this.cleanPOS()
        this.cleanPronunciation()
        this.cleanQuote()
        this.cleanSynonyms()
        this.cleanOrigins()
        this.cleanWord()
        this.cleanWordVariants()
        this.mLogos.uploadDate = new Date(Date.now());
        return this.mLogos;
    }

    /**
     * Validates a word
     * @param {LogosEntity} logos
     * @returns {string}
     */
    public validateWord(): string {
        if (this.mLogos.word.word === "" || !this.mLogos.word.word) {
            return "word cannot be empty or null";
        }
        return null;
    }

    public cleanWord() {
        this.mLogos.word.word.trim();
        const ans = LogosValidation.capitalizeFirstLetter(this.mLogos.word.word);
        this.mLogos.word.word = ans;
    }

    public validatePOS(): string {
        if (this.mLogos.pos == "" || !this.mLogos.pos) {
            return "POS cannot be empty";
        }
        return null;
    }

    public cleanPOS(){
        this.mLogos.pos.trim();
    }

    public validateBannerURL(): string {
        if(!this.mLogos.bannerURL || this.mLogos.bannerURL.source == "") {
            return null
        }else{
            if(this.mLogos.bannerURL.source.length < 6) {
                return "BannerURL cannot be less than 6 characters";
            }
        }
        return null;
    }

    public cleanBannerURL() {
        if(!this.mLogos.bannerURL || this.mLogos.bannerURL.source == "") {
            this.mLogos.bannerURL.source = "";
        }else {
            this.mLogos.bannerURL.source.trim();
        }
    }

    public validateImageURLs(): string {
        for(let i = 0; i < this.mLogos.imageURLs.length; i++) {
            if(this.mLogos.imageURLs[i].source != "") {
                if(this.mLogos.imageURLs[i].source.length < 6) {
                    return "imageURL must have length > 6 at index " + i;
                }
            }else {
                return null;
            }
        }
        return null;
    }

    public cleanImageURLs() {
        for(let i = 0; i < this.mLogos.imageURLs.length; i++) {
            if(this.mLogos.imageURLs[i].source.length > 5 ) {
                this.mLogos.imageURLs[i].source.trim()
            }
        }
        return this.mLogos.word;
    }

    public validateLanguages(): string {
        for(let i = 0; i < this.mLogos.languages.length; i++) {
            if(this.mLogos.languages[i].language === "") {
                if(this.mLogos.languages[i].language === "") {
                    return "language must have a source at index " + i;
                }
                return "language cannot be empty at index " + i;
            }
            if(this.mLogos.languages[i].language.length < 3) {
                return "language cannot have less than 3 characters at index" + i;
            }
        }
        return null;
    }

    public cleanLanguages() {
        for(let i = 0; i < this.mLogos.languages.length; i++) {
            if(this.mLogos.languages[i].language.length > 0) {
                this.mLogos.languages[i].language.trim();
                this.mLogos.languages[i].source.trim();
                this.mLogos.languages[i].language =  LogosValidation.capitalizeFirstLetter(this.mLogos.languages[i].language);
            }else{
                this.mLogos.languages[i] = null;
            }
        }
    }

    public validateOrigins(): string {
        for(let i = 0; i < this.mLogos.origins.length; i++) {
            if(this.mLogos.origins[i].origin === "") {
                if(this.mLogos.origins[i].source === "") {
                    return "origin must have a source at index " + i;
                }
                return "origin cannot be empty at index " + i;
            }
            if(this.mLogos.origins[i].origin.length < 3) {
                return "origin cannot have less than 3 characters at index" + i;
            }
        }
        return null;
    }

    public cleanOrigins() {
        for(let i = 0; i < this.mLogos.origins.length; i++) {
            if(this.mLogos.origins[i].origin.length > 0) {
                this.mLogos.origins[i].origin.trim();
                this.mLogos.origins[i].source.trim();
                this.mLogos.origins[i].origin =  LogosValidation.capitalizeFirstLetter(this.mLogos.origins[i].origin);
            }else{
                this.mLogos.origins[i] = null;
            }
        }
    }

    public validateDefinition(): string {
        for(let i = 0; i < this.mLogos.definitions.length; i++) {
            if(this.mLogos.definitions[i].definition === "") {
                if(this.mLogos.definitions[i].definition === "") {
                    return "definition must have a source at index " + i;
                }
                return "definition cannot be empty at index " + i;
            }
            if(this.mLogos.definitions[i].definition.length < 10) {
                return "definition cannot have less than 10 characters at index" + i;
            }
        }
        return null;
    }

    public cleanDefinition() {
        for(let i = 0; i < this.mLogos.definitions.length; i++) {
            if(this.mLogos.definitions[i].definition.length > 0) {
                this.mLogos.definitions[i].definition.trim();
                this.mLogos.definitions[i].source.trim();
                this.mLogos.definitions[i].definition =  LogosValidation.capitalizeFirstLetter(this.mLogos.definitions[i].definition);
            }else{
                this.mLogos.definitions[i] = null;
            }
        }
    }

    public validateExample(): string {
        let anecdoteEmpty: boolean = false;
        var anecdot = this.validateAnecdote();
        if(anecdot == null) {
            return null;
        }else {
            for(let i = 0; i < this.mLogos.examples.length; i++) {
                if(this.mLogos.examples[i].example === "") {
                    if(this.mLogos.examples[i].source === "") {
                        return "example must have a source at index " + i;
                    }
                    return "example cannot be empty at index " + i;
                }
            }
        }
        return null;
    }

    public cleanExample() {
        for(let i = 0; i < this.mLogos.examples.length; i++) {
            if(this.mLogos.examples[i].example.length > 0) {
                this.mLogos.examples[i].example.trim();
                this.mLogos.examples[i].source.trim();
                this.mLogos.examples[i].example =  LogosValidation.capitalizeFirstLetter(this.mLogos.examples[i].example);
            }else{
                this.mLogos.examples[i] = null;
            }
        }
    }

    public validateAnecdote(): string {
        for(let i = 0; i < this.mLogos.anecdotes.length; i++) {
            if(this.mLogos.anecdotes[i].anecdote === "") {
                if(this.mLogos.anecdotes[i].anecdote === "") {
                    return "anecdote must have a source at index " + i;
                }
                return "anecdote cannot be empty at index " + i;
            }
            if(this.mLogos.anecdotes[i].anecdote.length < 10) {
                return "anecdote cannot have less than 10 characters at index" + i;
            }
        }
        return null;
    }

    public cleanAnecdotes() {
        for(let i = 0; i < this.mLogos.anecdotes.length; i++) {
            if(this.mLogos.anecdotes[i].anecdote.length > 0) {
                this.mLogos.anecdotes[i].anecdote.trim();
                this.mLogos.anecdotes[i].source.trim();
                this.mLogos.anecdotes[i].anecdote =  LogosValidation.capitalizeFirstLetter(this.mLogos.anecdotes[i].anecdote);
            }else{
                this.mLogos.anecdotes[i] = null;
            }
        }
    }

    public validateQuotes(): string {
        return null;
    }

    public cleanQuote() {
        for(let i = 0; i < this.mLogos.quotes.length; i++) {

            if(this.mLogos.quotes[i].quote.length > 0) {
                this.mLogos.quotes[i].quote.trim();
                this.mLogos.quotes[i].source.trim();
                this.mLogos.quotes[i].quote =  LogosValidation.capitalizeFirstLetter(this.mLogos.quotes[i].quote);
            }
        }
    }

    public validateSynonyms(): string {
        for(let i = 0; i < this.mLogos.synonyms.length; i++) {
            if(this.mLogos.synonyms[i].synonym.length > 1) {
                if(this.mLogos.synonyms[i].source == "") {
                    return "synonym must have a source";
                }
                // return "synonym cannot be empty";
            }else{

            }
        }
        return null;
    }

    public cleanSynonyms() {
        for(let i = 0; i < this.mLogos.synonyms.length; i++) {
            this.mLogos.synonyms[i].synonym.trim();
            this.mLogos.synonyms[i].source.trim();
            this.mLogos.synonyms[i].synonym =  LogosValidation.capitalizeFirstLetter(this.mLogos.synonyms[i].synonym);
        }
    }

    public validateEtymology(): string {
        for(let i = 0; i < this.mLogos.etymologys.length; i++) {
            if(this.mLogos.etymologys[i].etymology === "") {
                if(this.mLogos.etymologys[i].source === "") {
                    return "etymology must have a source";
                }
                return "etymology cannot be empty";
            }
        }
        return null;
    }

    public cleanEtymologys() {
        for(let i = 0; i < this.mLogos.synonyms.length; i++) {
            this.mLogos.etymologys[i].etymology.trim();
            this.mLogos.etymologys[i].source.trim();
            this.mLogos.etymologys[i].etymology =  LogosValidation.capitalizeFirstLetter(this.mLogos.etymologys[i].etymology);
        }
    }

    public validateWordVariants(): string {
        return null;
    }

    public cleanWordVariants() {
        return null;
    }

    public validatePronunciations(): string {
        // for(let i = 0; i < this.mLogos.pronunciations.length; i++) {
        //
        // }
        // if (this.mLogos.pronunciations[0].pronunciation === "") {
        //     return "pronunciation cannot be empty";
        // }
        return null;
    }

    public cleanPronunciation(): IWord {
        this.mLogos.word.word.trim();
        return this.mLogos.word;
    }

    public static capitalizeFirstLetter(input: string): string {
        return input.charAt(0).toUpperCase() + input.slice(1);
    }
}
