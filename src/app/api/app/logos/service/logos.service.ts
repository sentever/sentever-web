import { LogosEntity } from './../entity/logos.entity';
import { Observable } from 'rxjs/Observable';
import { AngularFirestore } from 'angularfire2/firestore/public_api';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/elementAt'

export interface ILogosService {
  retrieveRecentLogos$(): Observable<LogosEntity>;
  retrieveLogosByKey?(key: string): Observable<LogosEntity>;
  retrieveLogosByKeys?(keys: Array<string>): Observable<LogosEntity>;
}


@Injectable()
export class LogosService implements ILogosService {
  constructor(private af: AngularFirestore) { }

/**
 * Retrieves the most recent available logos for today
 */
  public retrieveRecentLogos$(): Observable<LogosEntity> {
    return this.af.collection<LogosEntity>("logos", ref => ref
        .orderBy("availableDate", "desc")
        .limit(1))
            .valueChanges()
            .map(val => {
                return LogosService.getTodayAndPrevious(val)[0]
            });
  }

  /**
 * Retrieves the most recent available logos for today
 */
public retrieveRecentLogosWithLimit$(limit: number): Observable<LogosEntity[]> {
    return this.af.collection<LogosEntity>("logos", ref => ref
        .orderBy("availableDate", "desc")
        .limit(limit))
            .valueChanges();
  }

  /**
   * Returns the Logos Scheduled between 00:00:00 and 23:59:00
   */
  public retrieveTodaysLogos(): Observable<LogosEntity[]> {
      var midMorning = LogosService.getTodayMidMorning();
      var midNight = LogosService.getTodayMidnight();
      console.log(new Date(midMorning));
      console.log("MidMorning: " + midMorning);
      console.log("MidNight: " +midNight)
        return this.af.collection<LogosEntity>("logos", ref   => ref
                .where("availableDate", ">=",  new Date(midMorning * 1000))
                .where("availableDate", "<=",   new Date(midNight * 1000))
                .limit(10))
                    .valueChanges()
                    .map(val => {
                        console.log(val);
                        return LogosService.getToday(val)
                    });
  }

  /**
   * Returns an array of LogosEntities available before this point in time.
   * @param logosEntities
   */
  private static getTodayAndPrevious(logosEntities: LogosEntity[]): LogosEntity[] {
      const now = Date.now();
      let finArray = new Array<LogosEntity>();
      for (var i= 0; i < logosEntities.length; i++) {
          if(now > logosEntities[i].availableDate.getUTCMilliseconds()) {
            finArray.push(logosEntities[i]);
          }
      }
      return finArray;
  }

  /**
   * Returns an array of logos entities that are available only today between 00:00:00 and 23:59:00
   * @param logosEntities
   */
  private static getToday(logosEntities: LogosEntity[]): LogosEntity[] {
    const now = Date.now();
    var yesterdayMidnight = LogosService.getTodayMidMorning();
    let finArray = new Array<LogosEntity>();
    for (var i= 0; i < logosEntities.length; i++) {
        if(now > logosEntities[i].availableDate.getUTCMilliseconds() &&  logosEntities[i].availableDate.getUTCMilliseconds() > yesterdayMidnight) {
          finArray.push(logosEntities[i]);
        }
    }
    return finArray;
}

private static getTodayMidMorning(): number {
    var yesterdayMidnight = new Date();
    return (yesterdayMidnight.setHours(0,0,0,0)/1000)
}

private static getTodayMidnight(): number {
    var yesterdayMidnight = new Date();
    return (yesterdayMidnight.setHours(24,0,0,0)/1000)
}

}
