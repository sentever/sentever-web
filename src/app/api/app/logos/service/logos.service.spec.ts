import { AngularFirestore } from 'angularfire2/firestore/public_api';
import { LogosEntity } from './../entity/logos.entity';
/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LogosService } from './logos.service';
import { AngularFirestoreCollection } from 'angularfire2/firestore';

describe('LogosService', () => {
    let logosService: LogosService;
    let firestoreCollectionSpy: jasmine.SpyObj<AngularFirestore>;
    let angularFirestoreCollectionSpy: jasmine.SpyObj<AngularFirestoreCollection<any>>

    beforeEach(() => {
        const spy = jasmine.createSpyObj('AngularFirestore', ["collection"]);
        const afCollectionSpy = jasmine.createSpyObj('AngularFirestoreCollection', ["valueChanges"]);
        TestBed.configureTestingModule(
            {
                providers: [
                    LogosService, {provide: AngularFirestore, useValue: spy}
                    , {provide: AngularFirestoreCollection, useValue: afCollectionSpy}
                ]
             });
        logosService =TestBed.get(LogosService);
        firestoreCollectionSpy = TestBed.get(AngularFirestore);
        angularFirestoreCollectionSpy = TestBed.get(AngularFirestoreCollection);
    })

    it("#retrieveRecentLogos should return the most recent logos from Firestore", (done: DoneFn) => {
        debugger;
        logosService.retrieveRecentLogos$()
            .subscribe(value => {
                expect(value).toBeDefined("expect value to be defined");
                // expect(firestoreCollectionSpy.collection.calls.count())
                //     .toBe(1, "spyMethodCalledOnce")
                done();
            });
    });

    it("#retrieve", (done: DoneFn) => {
        logosService.retrieveTodaysLogos()
            .subscribe(val => {
                expect(firestoreCollectionSpy.collection.calls.count())
                     .toBe(1, "spyMethodCalledOnce")
                done();
            });
    })
})


// describe('Service: Logos', () => {
//   beforeEach(() => {
//     TestBed.configureTestingModule({
//       providers: [LogosService]
//     });
//   });

//   it('should ...', inject([LogosService], (service: LogosService) => {
//     expect(service).toBeTruthy();
//   }));
// });
