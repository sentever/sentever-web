import { LogosService } from './../../api/app/logos/service/logos.service';
import { CommonModule } from '@angular/common';
import { HomeRoutingModule } from './home.routing';

import { HomeComponent } from './home.component';

import { AngularFireStorageModule } from 'angularfire2/storage';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {AngularFireModule} from 'angularfire2';
import {AngularFirestoreModule} from 'angularfire2/firestore'
import {AngularFireAuthModule} from 'angularfire2/auth';

@NgModule({
  declarations: [
   HomeComponent
  ],
  imports: [
    CommonModule,
    HomeRoutingModule
  ],
  providers: [LogosService]
})
export class HomeModule { }
