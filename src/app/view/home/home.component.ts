import { Observable } from 'rxjs/Observable';
import { LogosEntity } from './../../api/app/logos/entity/logos.entity';
import { LogosService } from './../../api/app/logos/service/logos.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    logos$: Observable<LogosEntity>;

    constructor(private logosService: LogosService) {
        this.logos$ = this.logosService.retrieveRecentLogos$();
    }

    ngOnInit() {
    }
}
