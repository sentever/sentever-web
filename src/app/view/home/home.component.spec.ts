import { LogosService } from './../../api/app/logos/service/logos.service';
import { LogosEntity } from './../../api/app/logos/entity/logos.entity';
/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { HomeComponent } from './home.component';
import { Observable } from 'rxjs/Observable';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;
  let logosService: MockLogosService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent ],
      providers: [HomeComponent, {
          provide: LogosService, useClass: MockLogosService
      }]
    })
    .compileComponents();

    component = TestBed.get(HomeComponent);
    logosService = TestBed.get(LogosService)
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    TestBed.configureTestingModule({
        providers: [HomeComponent, {
            provide: LogosService, useClass: MockLogosService
        }]
    })
    component = fixture.componentInstance;
    logosService = TestBed.get(LogosService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should call retrieveRecentLogos", () => {
      expect(logosService.retrieveRecentLogos$().subscribe(val => {
          val.word.word = "test"
      }).
  });
});



class MockLogosService {
    public retrieveRecentLogos$(): Observable<LogosEntity> {
       let  logos: LogosEntity = new LogosEntity();
       logos.word = {
           word: "test",
           source: "testSource"
       }
        return Observable.of(logos);
    }
}
