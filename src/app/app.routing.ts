import { ModuleWithProviders } from '@angular/core/src/metadata/ng_module';
import { Routes, RouterModule } from '@angular/router';

export const ROUTES: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path:'',
        children:[
            {
              path: 'home',
              loadChildren:'./view/home/home.module#HomeModule'
          }
        ]
    }
];

export const AppRoutingModule: ModuleWithProviders = RouterModule.forRoot(ROUTES);
