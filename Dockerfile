FROM node:alpine
MAINTAINER "martin@sentever.com"

# install git
RUN apk update
RUN apk add openssh
RUN npm install -g @angular/cli

COPY . /sentever-web
WORKDIR /sentever-web
RUN npm install --production
EXPOSE 8080
CMD ["npm", "start", "--port", "8080"]
