FROM node:9.8.0-alpine
MAINTAINER "martin@sentever.com"

# install git
RUN apk update
RUN apk add git
RUN apk add openssh

# ARG SSH_PRIVATE_KEY
RUN mkdir /root/.ssh/
# RUN echo "${SSH_PRIVATE_KEY}" > /root/.ssh/id_rsa

# Copy over private key, and set permissions
# ADD id_rsa /root/.ssh/id_rsa
# RUN touch /root/.ssh/known_host && ssh-keyscan bitbucket.org >> /root/.ssh/known_hosts &&
#RUN git clone git@bitbucket.org:sentever/sentever-web.git
EXPOSE 8080

RUN git clone https://sentever@bitbucket.org/sentever/sentever-web.git
WORKDIR /sentever-web
RUN npm install
CMD ["npm", "start", "--port", "8080"]
