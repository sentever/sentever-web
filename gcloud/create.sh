#! bin/bash
gcloud container clusters create sentever-web --num-nodes=1 --preemptible && gcloud container clusters get-credentials sentever-web && kubectl run sentever-web --image=gcr.io/sentever-prod/sentever-web:1.0 --port=8080 && kubectl expose deployment sentever-web --type=LoadBalancer --target-port=8080 --port 80 && kubectl get services/sentever-web --watch

