#!/bin/bash
gcloud config set project sentever-prod && gcloud container clusters create sentever-web --num-nodes=1 --machine-type=n1-standard-1 --zone=europe-west1-d --preemptible && gcloud container clusters get-credentials sentever-web && kubectl run sentever-web --image=gcr.io/sentever-prod/sentever-web:1.0 --port 8080 && kubectl apply -f ingress.yaml

